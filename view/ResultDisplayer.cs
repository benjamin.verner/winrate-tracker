using WinRateTracker.model;
using System;
using System.Collections.Generic;
using System.Globalization;
using Spectre.Console;
using WinRateTracker.controller;

namespace WinRateTracker.view;

/// <summary>
/// Class for displaying results, sheets and statistics in the console. Uses Spectre.Console for nice formatting.
/// </summary>
public static class ResultDisplayer {
    /// <summary>
    /// Displays a single result in the console.
    /// </summary>
    /// <param name="result">Result object to be displayed <see cref="Result"/></param>
    public static void DisplayResult(Result result) {
        var data = new List<string> {
            $"Id: {result.Id}",
            $"Wins: {result.Wins}",
            $"Losses: {result.Losses}",
            $"Comments: {result.Comments}",
            $"Tags: {string.Join(", ", result.Tags)}",
            $"Timestamp: {result.Timestamp}"
        };

        var panel = new Panel(string.Join(Environment.NewLine, data))
            .Border(BoxBorder.Rounded)
            .BorderColor(Color.Blue);
        
        AnsiConsole.Write(panel);
    }

    /// <summary>
    /// Displays a single sheet in the console.
    /// </summary>
    /// <param name="sheet">Sheet object to be displayed <see cref="Sheet"/></param>
    public static void DisplaySheet(Sheet sheet) {
        var data = new List<string> {
            $"Id: {sheet.Id}",
            $"Name: {sheet.Name}",
            $"Spreadsheet ID: {sheet.SpreadsheetId}",
            $"Path to credentials: {sheet.PathToCredentials}"
        };
        
        var panel = new Panel(string.Join(Environment.NewLine, data))
            .Border(BoxBorder.Rounded)
            .BorderColor(Color.Blue);
        
        AnsiConsole.Write(panel);
    }

    /// <summary>
    /// Display statistics in the console. Uses values from the StatisticsCalculator object.
    /// </summary>
    /// <param name="sc">StatisticsCalculator objects with computed statistics values <see cref="StatisticsCalculator"/></param>
    public static void DisplayStatistics(StatisticsCalculator sc) {
        var table = new Table()
            .Border(TableBorder.Rounded)
            .BorderColor(Color.Blue)
            .HideHeaders()
            .ShowRowSeparators();

        table.AddColumn("");
        
        table.AddRow(new Markup($"[bold]Total matches:[/]: {sc.totalMatches}"));
        table.AddRow(new Markup($"[bold]Total match wins:[/]: {sc.totalMatchWins}"));
        table.AddRow(new Markup($"[bold]Total match losses:[/]: {sc.totalMatchLosses}"));
        table.AddRow(new Markup($"[bold]Match win rate:[/]: {sc.matchWinRate:F4}"));
        table.AddRow(new Markup($"[bold]Total games:[/]: {sc.totalGames}"));
        table.AddRow(new Markup($"[bold]Total game wins:[/]: {sc.totalGameWins}"));
        table.AddRow(new Markup($"[bold]Total game losses:[/]: {sc.totalGameLosses}"));
        table.AddRow(new Markup($"[bold]Game win rate:[/]: {sc.gameWinRate:F4}"));
        table.AddRow(new Markup($"[bold]Longest undefeated streak:[/]: {sc.longestUndefeatedStreak}"));
        if (_HasTags(sc)) {
            table.AddRow(_MostCommonTags(sc));
            table.AddRow(_WrapIntoTable(_CreateTagWinrateChart(sc)));
        }
        table.AddRow(_WrapIntoTable(_CreateWinrateChart(sc)));
        
        AnsiConsole.Write(table);
    }

    private static bool _HasTags(StatisticsCalculator sc) {
        return sc.tagMatches.Count > 0;
    }

    private static Tree _MostCommonTags(StatisticsCalculator sc) {
        var root = new Tree(new Markup($"Most common tags ({sc.mostCommonTags.Item2} games):"));
        
        foreach (string tag in sc.mostCommonTags.Item1) {
            root.AddNode(tag);
        }
        
        return root;
    }
    
    private static BarChart _CreateTagWinrateChart(StatisticsCalculator sc) {
        var chart = new BarChart()
            .Width(80)
            .Label("[green bold]WINRATE BY TAG[/]")
            .CenterLabel();
        
        // Sort tags by winrate
        var sortedTagWinrates = sc.tagWinRates
            .OrderByDescending(x => x.Value);
        
        var colors = new List<Color>{ Color.LightSlateBlue, Color.LightGreen };

        bool odd = true;
        foreach (var (tag, winrate) in sortedTagWinrates) {
            chart.AddItem($"{tag}", Math.Round(winrate, 4), odd ? colors[0] : colors[1]);
            odd = !odd;
        }
        
        return chart;
    }

    private static BarChart _CreateWinrateChart(StatisticsCalculator sc) {
        var chart = new BarChart()
            .Width(80)
            .Label("[green bold]WINRATE OVER TIME[/]")
            .CenterLabel();

        var colors = new List<Color>{ Color.DarkMagenta, Color.DarkViolet };

        bool odd = true;
        foreach (var (time, winrate) in sc.winrateOverMonths) {
            chart.AddItem($"{time.Item1}/{time.Item2} ({winrate.Item2} games)", Math.Round(winrate.Item1, 4), odd ? colors[0] : colors[1]);
            odd = !odd;
        }
        
        return chart;
    }

    private static Table _WrapIntoTable(BarChart item) {
        var table = new Table()
            .Border(TableBorder.Rounded)
            .BorderColor(Color.Green)
            .HideHeaders();

        table.AddColumn("");
        table.AddRow(item);
        return table;
    }
}