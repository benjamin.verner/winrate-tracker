namespace WinRateTracker.view;

/// <summary>
/// Class for displaying error messages to the console. Uses red text to make the error message stand out.
/// </summary>
public static class ErrorDisplayer {
    /// <summary>
    /// Displayes an error message to the console.
    /// </summary>
    /// <param name="message">Message to be displayed</param>
    public static void DisplayError(string message) {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(message);
        Console.ResetColor();
    }
}