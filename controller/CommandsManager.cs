namespace WinRateTracker.controller;

using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;

/// <summary>
/// Class managing subcommands and their handlers
/// </summary>
public class CommandsManager {
    private RootCommand rootCommand { get; } = new RootCommand();
    
    public CommandsManager() {
        rootCommand.Description = "WinRateTracker";
    }
    
    /// <summary>
    /// Run the manager with the provided arguments (invoking the handler of the corresponding subcommand)
    /// </summary>
    /// <param name="args">Arguments of the application</param>
    public void Run(string[] args) {
        rootCommand.Invoke(args);
    }
    
    /// <summary>
    /// Subcommand to create the database
    /// </summary>
    /// <param name="handler">Handler for the subcommand/param>
    public void CreateDb(Action handler) {
        Command createDbCommand = new Command(name: "create-db", description: "Create the database") {
        };
        
        createDbCommand.SetHandler(() => {
            handler();
        });
        
        rootCommand.AddCommand(createDbCommand);
    }

    /// <summary>
    /// Subcommand to get a specific result by ID
    /// </summary>
    /// <param name="handler">Handler for the subcommand</param>
    public void Get(Action<int> handler) {
        var idArg = new Argument<int>("id", "") { Arity = ArgumentArity.ExactlyOne };
        
        Command getCommand = new Command(name: "get", description: "Get a specific result by ID") {
            idArg
        };
        
        getCommand.SetHandler((id) => {
            handler(id);
        }, idArg);
        
        rootCommand.AddCommand(getCommand);
    }
    
    /// <summary>
    /// Subcommand to add a result to the database
    /// </summary>
    /// <param name="handler">Handler for the subcommand</param>
    public void Add(Action<int, int, string, List<string>> handler) {
        var winsOption = new Argument<int>("wins", "Number of wins") { Arity = ArgumentArity.ExactlyOne };
        var lossesOption = new Argument<int>("losses", "Number of losses") { Arity = ArgumentArity.ExactlyOne };
        var commentOption = new Option<string>("--comment", "Comment (string)") { IsRequired = false };
        var tagsOption = new Option<List<string>>("--tags", "Tags (string)") {
            IsRequired = false, AllowMultipleArgumentsPerToken = true
        };
        
        commentOption.SetDefaultValue("");
        tagsOption.SetDefaultValue(new List<string>());
        
        Command addCommand = new Command(name: "add", description: "Add result to the database") {
            winsOption,
            lossesOption,
            commentOption,
            tagsOption
        };
        
        addCommand.SetHandler( (wins, losses, comment, tags) => {
            handler(wins, losses, comment, tags);
        }, winsOption, lossesOption, commentOption, tagsOption);
        
        rootCommand.AddCommand(addCommand);
    }

    /// <summary>
    /// Subcommand to list results from the database
    /// </summary>
    /// <param name="handler">Handler for the subcommand</param>
    public void List(Action<string?, List<string>?, int?, int?, int?, string?> handler) {
        var resultOption = new Option<string>("--result", "Filter 'win', 'loss' or 'draw' results, operator '+', e.g. 'win+draw'") {
            IsRequired = false
        };
        var tagsOption = new Option<List<string>?>("--tags", "Filter by tag") {
            IsRequired = false,
            AllowMultipleArgumentsPerToken = true
        };
        var yearOption = new Option<int?>("--year", "Filter by year") { IsRequired = false };
        var monthOption = new Option<int?>("--month", "Filter by month") { IsRequired = false };
        var dayOption = new Option<int?>("--day", "Filter by day") { IsRequired = false };
        var commentOption = new Option<string?>("--comment", "Filter by comment (phrase included in the comment, not exact match)") {
            IsRequired = false
        };
        
        Command listCommand = new Command(name: "list", description: "List results filtered by options") {
            resultOption,
            tagsOption,
            yearOption,
            monthOption,
            dayOption,
            commentOption,
        };
        
        listCommand.SetHandler((result, tags, year, month, day, comment) => {
            handler(result, tags, year, month, day, comment);
        }, resultOption, tagsOption, yearOption, monthOption, dayOption, commentOption);
        
        rootCommand.AddCommand(listCommand);
    }

    /// <summary>
    /// Subcommand to update a result in the database
    /// </summary>
    /// <param name="handler">Handler for the subcommand</param>
    public void Update(Action<int, bool, int?, int?, string?, List<string>, List<string>> handler) {
        var idArg = new Argument<int>("id", "") { Arity = ArgumentArity.ExactlyOne };
        var deleteOption = new Option<bool>("--delete", "Delete the result") { IsRequired = false };
        var winsOption = new Option<int?>("--wins", "Number of wins") { IsRequired = false };
        var lossesOption = new Option<int?>("--losses", "Number of losses") { IsRequired = false };
        var commentOption = new Option<string?>("--comment", "Comment (string)") { IsRequired = false };
        var addTagsOption = new Option<List<string>>("--add-tags", "Tags (string)") {
            IsRequired = false,
            AllowMultipleArgumentsPerToken = true
        };
        var replaceTagsOption = new Option<List<string>>("--replace-tags", "Tags (string)") {
            IsRequired = false,
            AllowMultipleArgumentsPerToken = true
        };
        
        deleteOption.SetDefaultValue(false);
        addTagsOption.SetDefaultValue(new List<string>());
        replaceTagsOption.SetDefaultValue(new List<string>());
        
        Command updateCommand = new Command(name: "update", description: "Update result in the database") {
            idArg,
            deleteOption,
            winsOption,
            lossesOption,
            commentOption,
            addTagsOption,
            replaceTagsOption
        };
        
        updateCommand.AddValidator(result => {
            var addTags = result.GetValueForOption(addTagsOption);
            var replaceTags = result.GetValueForOption(replaceTagsOption);
            
            if (addTags != null && replaceTags != null && addTags.Count > 0 && replaceTags.Count > 0) {
                result.ErrorMessage = "Cannot add and replace tags at the same time";
            }
        });
        
        updateCommand.SetHandler((id, delete, wins, losses, comment, addTags, replaceTags) => {
            handler(id, delete, wins, losses, comment, addTags, replaceTags);
        }, idArg, deleteOption, winsOption, lossesOption, commentOption, addTagsOption, replaceTagsOption);
        
        rootCommand.AddCommand(updateCommand);
    }

    /// <summary>
    /// Subcommand to display statistics
    /// </summary>
    /// <param name="handler">Handler for the subcommand</param>
    public void Stats(Action handler) {
        Command statsCommand = new Command(name: "stats", description: "Show statistics") { };
        
        statsCommand.SetHandler(() => {
            handler();
        });
        
        rootCommand.AddCommand(statsCommand);
    }

    /// <summary>
    /// Subcommand to bind a Google Sheet to the database for sync
    /// </summary>
    /// <param name="handler">Handler for the subcommand</param>
    public void BindSheet(Action<string, string, string> handler) {
        var nameArg = new Argument<string>("name", "Name of the spreadsheet") { Arity = ArgumentArity.ExactlyOne };
        var idArg = new Argument<string>("id", "Spreadsheet ID") { Arity = ArgumentArity.ExactlyOne };
        var pathArg = new Argument<string>("path", "Path to the credentials file") { Arity = ArgumentArity.ExactlyOne };
        
        var bindCommand = new Command(name: "bind-sheet", description: "Bind a sheet to the database for sync") {
            nameArg,
            idArg,
            pathArg
        };
        
        bindCommand.SetHandler((name, id, path) => {
            handler(name, id, path);
        }, nameArg, idArg, pathArg);
        
        rootCommand.AddCommand(bindCommand);
    }

    /// <summary>
    /// Subcommand to list all sheets remembered for sync
    /// </summary>
    /// <param name="handler">Handler for the subcommand</param>
    public void ListSheets(Action handler) {
        var listSheetsCommand = new Command(name: "list-sheets", description: "List all sheets available for sync") { };
        
        listSheetsCommand.SetHandler(() => {
            handler();
        });
        
        rootCommand.AddCommand(listSheetsCommand);
    }
    
    /// <summary>
    /// Subcommand to remove a sheet from the database sync options list
    /// </summary>
    /// <param name="handler">Handler for the subcommand</param>
    public void RemoveSheet(Action<int> handler) {
        var idArg = new Argument<int>("id", "") { Arity = ArgumentArity.ExactlyOne };
        
        var removeSheetCommand = new Command(name: "remove-sheet", description: "Remove a sheet from the database sync") {
            idArg
        };
        
        removeSheetCommand.SetHandler((id) => {
            handler(id);
        }, idArg);
        
        rootCommand.AddCommand(removeSheetCommand);
    }
    
    /// <summary>
    /// Subcommand to mirror the sync sheet to the database
    /// </summary>
    /// <param name="handler">Handler for the subcommand</param>
    public void MirrorSheet(Action<int?, string?> handler) {
        var idArg = new Argument<int?>("id", "") { Arity = ArgumentArity.ZeroOrOne };
        var nameArg = new Argument<string?>("name", "") { Arity = ArgumentArity.ZeroOrOne };
        
        var mirrorCommand = new Command(name: "mirror-sheet", description: "Mirror the sync sheet to the database (either provide ID or NAME of the sheet)") {
            idArg,
            nameArg
        };
        
        mirrorCommand.AddValidator(result => {
            var id = result.GetValueForArgument(idArg);
            var name = result.GetValueForArgument(nameArg);
            
            if (id == null && name == null) {
                result.ErrorMessage = "Either ID or NAME must be provided";
            }

            if (id != null && name != null) {
                result.ErrorMessage = "Cannot provide both ID and NAME";
            }
        
        });
        
        mirrorCommand.SetHandler((id, arg) => {
            handler(id, arg);
        }, idArg, nameArg);
        
        rootCommand.AddCommand(mirrorCommand);
    }

    /// <summary>
    /// Subcommand to write the database content to the sync sheet
    /// </summary>
    /// <param name="handler">Handler for the subcommand</param>
    public void WriteToSheet(Action<int?, string?> handler) {
        var idArg = new Argument<int?>("id", "") { Arity = ArgumentArity.ZeroOrOne };
        var nameArg = new Argument<string?>("name", "") { Arity = ArgumentArity.ZeroOrOne };
        
        var writeToSheetCommand = new Command(name: "write-to-sheet", description: "Write the database content to the sync sheet") {
            idArg,
            nameArg
        };
        
        writeToSheetCommand.AddValidator(result => {
            var id = result.GetValueForArgument(idArg);
            var name = result.GetValueForArgument(nameArg);
            
            if (id == null && name == null) {
                result.ErrorMessage = "Either ID or NAME must be provided";
            }

            if (id != null && name != null) {
                result.ErrorMessage = "Cannot provide both ID and NAME";
            }
        
        });
        
        writeToSheetCommand.SetHandler((id, name) => {
            handler(id, name);
        }, idArg, nameArg);
        
        rootCommand.AddCommand(writeToSheetCommand);
    }
}