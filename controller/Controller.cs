using WinRateTracker.model;
using WinRateTracker.sync;
using WinRateTracker.view;

namespace WinRateTracker.controller;

/// <summary>
/// Controller class for the WinRateTracker application. Communicates with Model and View.
/// Includes handlers for all commands.
/// </summary>
public class Controller {
    private Model _model = new ();
    private StatisticsCalculator _stats = new ();
    public int exitCode = 0;
    
    /// <summary>
    /// Database creation handler
    /// </summary>
    public void CreateDb() {
        _model.CreateDb();
    }
    
    /// <summary>
    /// Result retrieval handler
    /// </summary>
    /// <param name="id">ID of the result to be retrieved</param>
    public void GetResult(int id) {
        _ErrorIfMissingDb();
        
        Result? result = _model.GetResult(id);
        if (result == null) {
            ErrorDisplayer.DisplayError($"No result found with id {id}!");
            exitCode = 1;
        }
        else {
            ResultDisplayer.DisplayResult(result);
        }
    }
    
    /// <summary>
    /// Handler for adding a result to the database
    /// </summary>
    /// <param name="wins">Number of wins</param>
    /// <param name="losses">Number of losses</param>
    /// <param name="comments">Comments</param>
    /// <param name="tags">List of tags for the result</param>
    public void AddResult(int wins, int losses, string comments, List<string> tags) {
        _ErrorIfMissingDb();
        
        _model.AddResult(wins, losses, comments, tags);
        _model.Save();
    }

    /// <summary>
    /// Handler for listing results from the database
    /// </summary>
    /// <param name="result">String determining the result (win/loss/draw), + operator allowed</param>
    /// <param name="tags">List of tag filters</param>
    /// <param name="year">Year filter</param>
    /// <param name="month">Month filter</param>
    /// <param name="day">Day filter</param>
    /// <param name="comments">Comment filter (inclusion, not exact match)</param>
    public void ListResults(string? result, List<string>? tags, int? year, int? month, int? day, string? comments) {
        _ErrorIfMissingDb();
        
        List<Result> results = _model.ListResults(result, tags, year, month, day, comments);
        foreach (Result match in results) {
            ResultDisplayer.DisplayResult(match);
        }
        
    }

    /// <summary>
    /// Result updating handler
    /// </summary>
    /// <param name="id">ID of the result to update</param>
    /// <param name="delete">If the result should be deleted instead</param>
    /// <param name="wins">Updated number of wins</param>
    /// <param name="losses">Updated number of losses</param>
    /// <param name="comments">Updated comments</param>
    /// <param name="addTags">Tags to add</param>
    /// <param name="replaceTags">Tags to replace the old tags with</param>
    public void UpdateResult(int id, bool delete, int? wins, int? losses, string? comments, List<string> addTags, 
        List<string> replaceTags) {
        _ErrorIfMissingDb();
        
        var result = _model.GetResult(id);
        
        if (result == null) {
            ErrorDisplayer.DisplayError($"No result found with id {id}!");
            exitCode = 1;
        }
        else {
            if (delete) {
                _model.DeleteResult(result);
            }
            else {
                if (wins != null) {
                    result.Wins = (int) wins;
                }
                if (losses != null) {
                    result.Losses = (int) losses;
                }
                if (comments != null) {
                    result.Comments = comments;
                }
                foreach (string tag in addTags) {
                    result.AddTag(tag);
                }
                foreach (string tag in replaceTags) {
                    result.Tags.Clear();
                    result.AddTag(tag);
                }
            }
            _model.Save();
        }
    }

    /// <summary>
    /// Display statistics handler
    /// </summary>
    public void Stats() {
        _ErrorIfMissingDb();

        if (_model.DbEmpty()) {
            ErrorDisplayer.DisplayError("No results in the database!");
            exitCode = 1;
            return;
        }
        
        _stats.UpdateStats(_model.Results);
        ResultDisplayer.DisplayStatistics(_stats);
    }

    /// <summary>
    /// Sheet binding handler
    /// </summary>
    /// <param name="sheetName">Name of the bind entry</param>
    /// <param name="spreadsheetId">ID of the Google Spreadsheet</param>
    /// <param name="pathToCredentials">Path to JSON credentials</param>
    public void BindSheet(string sheetName, string spreadsheetId, string pathToCredentials) {
        _ErrorIfMissingDb();
        
        _model.AddSheet(sheetName, spreadsheetId, pathToCredentials);
        _model.Save();
    }

    /// <summary>
    /// Sheet removal handler
    /// </summary>
    /// <param name="id">ID of the bind entry to remove</param>
    public void RemoveSheet(int id) {
        _ErrorIfMissingDb();
        
        _model.DeleteSheet(id);
        _model.Save();
    }

    /// <summary>
    /// Handler for listing all sheet binds
    /// </summary>
    public void ListSheets() {
        _ErrorIfMissingDb();
        
        foreach (Sheet sheet in _model.Sheets) {
            ResultDisplayer.DisplaySheet(sheet);
        }
    }
    
    private void _ErrorIfMissingDb() {
        if (!_model.DbExists()) {
            ErrorDisplayer.DisplayError("Database does not exist! Run 'create-db' first.");
            Environment.Exit(1);
        }
    }

    private Sheet? _GetSheet(int? id, string? name) {
        var sheets = new List<Sheet>();
        
        if (id != null) {
            var sheet = _model.GetSheet(id.Value);
            if (sheet == null) {
                ErrorDisplayer.DisplayError($"No sheet found with id {id}!");
                exitCode = 1;
                return null;
            }
            sheets.Add(sheet);
        }
        
        if (name != null) {
            sheets = _model.GetSheets(name);
        }

        if (sheets.Count == 0) {
            ErrorDisplayer.DisplayError("No sheets found with the given name!");
            exitCode = 1;
            return null;
        }
        else if (sheets.Count > 1) {
            ErrorDisplayer.DisplayError("Multiple sheets found with the same name!");
            exitCode = 1;
            return null;
        }

        return sheets[0];
    }

    /// <summary>
    /// Handler for mirroring a sheet to the local database
    /// (exactly one of id and name must be provided)
    /// </summary>
    /// <param name="id">ID of the sheet bind</param>
    /// <param name="name">Name of the sheet bind</param>
    public void MirrorSheet(int? id, string? name) {
        var sheet = _GetSheet(id, name);
        if (sheet == null) {
            return;
        }
        
        GoogleSheetsSyncer syncer = new GoogleSheetsSyncer(sheet.Name, sheet.SpreadsheetId, sheet.PathToCredentials);
        Dictionary<int, Result> results;

        try {
            results = syncer.GetResultsFromSheet();
        } catch (SyncException e) {
            ErrorDisplayer.DisplayError(e.Message);
            exitCode = 1;
            return;
        }

        _model.WipeDb();
        
        foreach (Result result in results.Values) {
            _model.AddResult(result);
        }
        
        _model.Save();
    }

    /// <summary>
    /// Handler for writing database content to a sheet
    /// (exactly one of id and name must be provided)
    /// </summary>
    /// <param name="id">ID of the sheet bind</param>
    /// <param name="name">Name of the sheet bind</param>
    public void WriteToSheet(int? id, string? name) {
        var sheet = _GetSheet(id, name);
        if (sheet == null) {
            return;
        }
        
        GoogleSheetsSyncer syncer = new GoogleSheetsSyncer(sheet.Name, sheet.SpreadsheetId, sheet.PathToCredentials);
        
        // Get the results from the sheet and the local database
        var results = syncer.GetResultsFromSheet();
        var localResults = _model.Results;
        
        // Compare them and keep track of sheet rows to preserve and IDs of results contained in them
        var rowsToKeep = new HashSet<int>();
        var idsAlreadyInSheet = new HashSet<int>();
        
        foreach (var (row, result) in results) {
            var correspondingLocalResult = localResults.Find(x => x.Id == result.Id);

            if (correspondingLocalResult != null && result == correspondingLocalResult) {
                rowsToKeep.Add(row);
                idsAlreadyInSheet.Add(result.Id);
            }
        }
        
        // Filter rows to write to the sheet and assign free rows to them
        var rowsToWrite = new Dictionary<int, Result>();
        int nextFreeRow = 1;
        
        foreach (var result in localResults) {
            if (idsAlreadyInSheet.Contains(result.Id)) {
                continue;
            }
            
            while (rowsToKeep.Contains(nextFreeRow)) {
                nextFreeRow++;
            }
            
            rowsToWrite[nextFreeRow] = result;
            nextFreeRow++;
        }

        try {
            // Write the results to the sheet
            syncer.WriteResultsToSheet(rowsToWrite);
        } catch (SyncException e) {
            ErrorDisplayer.DisplayError(e.Message);
            exitCode = 1;
        }
    }
}