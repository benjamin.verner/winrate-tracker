using WinRateTracker.model;

namespace WinRateTracker.controller;

/// <summary>
/// Class for calculating statistics from a list of results.
/// </summary>
public class StatisticsCalculator {
    private List<Result> _results = new List<Result>();
    /// <summary>
    /// Total number of matches (results) in the database
    /// </summary>
    public int totalMatches = 0;
    /// <summary>
    /// Total number of games in the database (a result has wins+losses games)
    /// </summary>
    public int totalGames = 0;
    /// <summary>
    /// Total number of games won
    /// </summary>
    public int totalGameWins = 0;
    /// <summary>
    /// Total number of games lost
    /// </summary>
    public int totalGameLosses = 0;
    /// <summary>
    /// Total number of matches won
    /// </summary>
    public int totalMatchWins = 0;
    /// <summary>
    /// Total number of matches lost
    /// </summary>
    public int totalMatchLosses = 0;
    /// <summary>
    /// Match win rate (matches won / all non-draw matches)
    /// </summary>
    public float matchWinRate = 0;
    /// <summary>
    /// Game win rate (games won / all games)
    /// </summary>
    public float gameWinRate = 0;
    /// <summary>
    /// Number of matches with each tag (tag, count)
    /// </summary>
    public Dictionary<string, int> tagMatches = new Dictionary<string, int>();
    /// <summary>
    /// Match win rate for each tag (tag, win rate)
    /// </summary>
    public Dictionary<string, float> tagWinRates = new Dictionary<string, float>();
    /// <summary>
    /// All tags that are the most common (most matches played), and the number of matches with those tags
    /// </summary>
    public Tuple<HashSet<string>, int> mostCommonTags = new Tuple<HashSet<string>, int>(new HashSet<string>(), 0);
    /// <summary>
    /// Longest streak of matches won without a loss (draws do not break the streak)
    /// </summary>
    public int longestUndefeatedStreak = 0;
    /// <summary>
    /// Match win rate for each month (year, month, win rate, number of games)
    /// </summary>
    public Dictionary<Tuple<int, int>, Tuple<double, int>> winrateOverMonths = new Dictionary<Tuple<int, int>, Tuple<double, int>>();

    /// <summary>
    /// Update the statistics with a new list of results
    /// </summary>
    /// <param name="results">List of results from the database</param>
    public void UpdateStats(List<Result> results) {
        _results = results;
        _UpdateTotalMatches();
        _UpdateTotalGames();
        _TotalGameWins();
        _TotalGameLosses();
        _TotalMatchWins();
        _TotalMatchLosses();
        _CalculateMatchWinRate();
        _CalculateGameWinRate();
        _CalculateTagWinRates();
        _CalculateMostCommonTags();
        _CalculateLongestStreaks();
        _UpdateWinrateOverMonths();
    }
    
    private void _UpdateTotalMatches() {
        totalMatches = _results.Count;
    }
    
    private void _UpdateTotalGames() {
        foreach (Result result in _results) {
            totalGames += result.Wins + result.Losses;
        }
    }
    
    private void _TotalGameWins() {
        foreach (Result result in _results) {
            totalGameWins += result.Wins;
        }
    }
    
    private void _TotalGameLosses() {
        foreach (Result result in _results) {
            totalGameLosses += result.Losses;
        }
    }
    
    private void _TotalMatchWins() {
        foreach (Result result in _results) {
            if (result.IsWin()) {
                totalMatchWins++;
            }
        }
    }
    
    private void _TotalMatchLosses() {
        foreach (Result result in _results) {
            if (result.IsLoss()) {
                totalMatchLosses++;
            }
        }
    }
    
    private void _CalculateMatchWinRate() {
        matchWinRate = (float) totalMatchWins / (totalMatchWins + totalMatchLosses);
    }
    
    private void _CalculateGameWinRate() {
        gameWinRate = (float) totalGameWins / (totalGameWins + totalGameLosses);
    }
    
    private void _CalculateTagWinRates() {
        var _tagDraws = new Dictionary<string, int>();
        foreach (Result result in _results) {
            foreach (string tag in result.Tags) {
                if (!tagMatches.TryAdd(tag, 1)) {
                    tagMatches[tag]++;
                }
                
                if (!tagWinRates.TryAdd(tag, result.IsWin() ? 1 : 0)) {
                    tagWinRates[tag] += result.IsWin() ? 1 : 0;
                }
                
                if (!_tagDraws.TryAdd(tag, result.isDraw() ? 1 : 0)) {
                    _tagDraws[tag] += result.isDraw() ? 1 : 0;
                }
            }
        }
        
        foreach (string tag in tagWinRates.Keys) {
            tagWinRates[tag] /= tagMatches[tag] - _tagDraws[tag];
        }
        
    }

    private void _CalculateMostCommonTags() {
        int max = 0;
        HashSet<string> mostCommonTags = new HashSet<string>();
        foreach (var (tag, count) in tagMatches) {
            if (count > max) {
                max = count;
                mostCommonTags.Clear();
                mostCommonTags.Add(tag);
            }
            else if (count == max) {
                mostCommonTags.Add(tag);
            }
        }
        
        this.mostCommonTags = new Tuple<HashSet<string>, int>(mostCommonTags, max);
    }
    
    private void _CalculateLongestStreaks() {
        int currentUndefeatedStreak = 0;
        
        // sort results by timestamp
        _results.Sort((a, b) => a.Timestamp.CompareTo(b.Timestamp));
        
        foreach (Result result in _results) {
            if (result.IsWin()) {
                currentUndefeatedStreak++;
            }
            else if (result.IsLoss()) {
                currentUndefeatedStreak = 0;
            }
            
            if (currentUndefeatedStreak > longestUndefeatedStreak) {
                longestUndefeatedStreak = currentUndefeatedStreak;
            }
        }
    }
    
    private Tuple<int, int> _GetMonthYear(Result result) {
        return new Tuple<int, int>(result.Timestamp.Year, result.Timestamp.Month);
    }

    private void _UpdateWinrateOverMonths() {
        var gamesPerMonth = new Dictionary<Tuple<int, int>, Tuple<int, int, int>>();
        foreach (Result result in _results) {
            var monthAndYear = _GetMonthYear(result);
            int win = result.IsWin() ? 1 : 0;
            int loss = result.IsLoss() ? 1 : 0;
            int draw = result.isDraw() ? 1 : 0;
            
            if (!gamesPerMonth.TryAdd(monthAndYear, new Tuple<int, int, int>(win, loss, draw))) {
                var (wins, losses, draws) = gamesPerMonth[monthAndYear];
                gamesPerMonth[monthAndYear] = new Tuple<int, int, int>(wins + win, losses + loss, draws + draw);
            }
        }
        
        foreach (var (monthAndYear, (wins, losses, draws)) in gamesPerMonth) {
            winrateOverMonths[monthAndYear] = new Tuple<double, int>((double) wins / (wins + losses), wins + losses + draws);
        }
    }
}