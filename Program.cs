﻿using WinRateTracker.controller;

namespace WinRateTracker;

using System.CommandLine;

/// <summary>
/// Class that binds commands to the Controller and contains the main entry point for the application.
/// </summary>
class Program
{
    /// <summary>
    /// Bind the commands to the controller.
    /// </summary>
    /// <param name="controller">Controller of the application <see cref="Controller"/></param>
    /// <returns>CommandsManager that can be then run to invoke a specific command handler <see cref="CommandsManager"/></returns>
    static CommandsManager initializeCommandsManager(Controller controller) {
        CommandsManager commandsManager = new CommandsManager();
        commandsManager.CreateDb(controller.CreateDb);
        commandsManager.Get(controller.GetResult);
        commandsManager.Add(controller.AddResult);
        commandsManager.List(controller.ListResults);
        commandsManager.Update(controller.UpdateResult);
        commandsManager.Stats(controller.Stats);
        commandsManager.BindSheet(controller.BindSheet);
        commandsManager.RemoveSheet(controller.RemoveSheet);
        commandsManager.ListSheets(controller.ListSheets);
        commandsManager.MirrorSheet(controller.MirrorSheet);
        commandsManager.WriteToSheet(controller.WriteToSheet);
        return commandsManager;
    }
    
    /// <summary>
    /// Main entry point for the application.
    /// </summary>
    /// <param name="args">Arguments of the program</param>
    /// <returns>Exit code</returns>
    static int Main(string[] args)
    {
        Controller controller = new Controller();
        CommandsManager commandsManager = initializeCommandsManager(controller);
        commandsManager.Run(args);
        return controller.exitCode;
    }
}
