#pragma warning disable CS8604

using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace WinRateTracker.model;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;

/// <summary>
/// Context for the database. Provides access to the database and the tables within it.
/// </summary>
public class MTGContext : DbContext {
    /// <summary>
    /// Results table
    /// </summary>
    public DbSet<Result> Results { get; set; }
    /// <summary>
    /// Sheets table
    /// </summary>
    public DbSet<Sheet> Sheets { get; set; }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
        optionsBuilder.UseSqlite("Data Source=mtg.db");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        modelBuilder.Entity<Result>().Property(r => r.Id).ValueGeneratedOnAdd();
        modelBuilder.Entity<Result>().Property(r => r.Wins).IsRequired();
        modelBuilder.Entity<Result>().Property(r => r.Losses).IsRequired();
        modelBuilder.Entity<Result>().Property(r => r.Timestamp).ValueGeneratedOnAdd();
        modelBuilder.Entity<Result>().Property(r => r.Comments).IsRequired(false);
        
        modelBuilder.Entity<Result>().Property(r => r.Tags)
            .HasConversion(
                v => String.Join(',', v),
                v => v.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList()
            )
            .Metadata.SetValueComparer(new ValueComparer<ICollection<string>>(
                (c1, c2) => c1.SequenceEqual(c2),
                c => c.Aggregate(0, (a, v) => HashCode.Combine(a, v.GetHashCode())),
                c => c.ToList()
            ));
        
        modelBuilder.Entity<Sheet>().Property(s => s.Id).ValueGeneratedOnAdd();
        modelBuilder.Entity<Sheet>().Property(s => s.Name).IsRequired();
        modelBuilder.Entity<Sheet>().Property(s => s.PathToCredentials).IsRequired();
    }
}