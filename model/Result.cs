#pragma warning disable CS8600
#pragma warning disable CS8602
#pragma warning disable CS8604
#pragma warning disable CS8618

namespace WinRateTracker.model;

using System;
using System.Collections.Generic;

/// <summary>
/// Class representing a result of a game.
/// </summary>
public class Result {
    /// <summary>
    /// ID of the result
    /// </summary>
    public int Id { get; init; }
    /// <summary>
    /// Number of wins in the result
    /// </summary>
    public int Wins;
    /// <summary>
    /// Number of losses in the result
    /// </summary>
    public int Losses;
    /// <summary>
    /// Time of the result creation
    /// </summary>
    public DateTime Timestamp = DateTime.Now;
    /// <summary>
    /// Comments about the result
    /// </summary>
    public string Comments;
    /// <summary>
    /// Tags associated with the result
    /// </summary>
    public ICollection<string> Tags { get; init; } = new List<string>();
    
    public Result() { }
    
    public Result(int wins, int losses, string comments, List<string> tags) {
        Wins = wins;
        Losses = losses;
        Comments = comments;
        Tags = tags ?? new List<string>();
    }

    /// <summary>
    /// Converts a list of objects from a Google Sheets row to a Result object
    /// </summary>
    /// <param name="row">Row as received from a sheet</param>
    /// <returns>New Result object <see cref="Result"/></returns>
    public static Result FromSheetRow(IList<object> row) {
        int id = int.Parse(row[0].ToString());
        int wins = int.Parse(row[1].ToString());
        int losses = int.Parse(row[2].ToString());
        DateTime timestamp = DateTime.Parse(row[3].ToString());
        string comments = row[4].ToString();
        List<string> tags = new List<string>(row[5].ToString().Split(','));
        
        return new Result(wins, losses, comments, tags) {
            Id = id,
            Timestamp = timestamp,
        };
    }
    
    /// <summary>
    /// Add an additional tag to the result
    /// </summary>
    /// <param name="tag">Tag to add</param>
    public void AddTag(string tag) {
        Tags.Add(tag);
    }

    /// <summary>
    /// Check if the result is a win
    /// </summary>
    /// <returns>Bool flag indicating if the result is a win</returns>
    public bool IsWin() {
        return Wins > Losses;
    }

    /// <summary>
    /// Check if the result is a loss
    /// </summary>
    /// <returns>Bool flag indicating if the result is a loss</returns>
    public bool IsLoss() {
        return Wins < Losses;
    }

    /// <summary>
    /// Check if the result is a draw
    /// </summary>
    /// <returns>Bool flag indicating if the result is a draw</returns>
    public bool isDraw() {
        return Wins == Losses;
    }
}