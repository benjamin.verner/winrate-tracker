using WinRateTracker.view;

namespace WinRateTracker.model;

/// <summary>
/// Model class for the Win Rate Tracker application. Handles all data storage and retrieval.
/// </summary>
public class Model : IDisposable {
    private readonly MTGContext _context = new ();

    public Model() { }
    
    /// <summary>
    /// Create the database file if it does not already exist
    /// </summary>
    public void CreateDb() {
        if (!File.Exists("mtg.db")) {
            File.Create("mtg.db");
            _context.Database.EnsureCreated();
        }
    }
    
    /// <summary>
    /// Check if the database file exists
    /// </summary>
    /// <returns>Bool existence flag</returns>
    public bool DbExists() {
        return File.Exists("mtg.db");
    }
    
    /// <summary>
    /// Check if the database is empty
    /// </summary>
    /// <returns>Bool emptyness flag</returns>
    public bool DbEmpty() {
        return !_context.Results.Any();
    }
    
    /// <summary>
    /// Remove all result entries from the database
    /// </summary>
    public void WipeDb() {
        _context.Results.RemoveRange(_context.Results);
        Save();
    }
    
    /// <summary>
    /// Get all results from the database
    /// </summary>
    public List<Result> Results {
        get {
            return _context.Results.ToList();
        }
    }

    /// <summary>
    /// Get all sheets from the database
    /// </summary>
    public List<Sheet> Sheets {
        get {
            return _context.Sheets.ToList();
        }
    }

    /// <summary>
    /// Get a result from the database by its ID
    /// </summary>
    /// <param name="id">ID of the result</param>
    /// <returns>The result if such exists, else null</returns>
    public Result? GetResult(int id) {
        List<Result> results = _context.Results.Where(x => x.Id == id).ToList();
        if (results.Count == 0) {
            return null;
        }
        else {
            return results[0];
        }
    }
    
    /// <summary>
    /// Get a sheet from the database by its ID
    /// </summary>
    /// <param name="id">ID of the sheet</param>
    /// <returns>The sheet if such exists, else null</returns>
    public Sheet? GetSheet(int id) {
        List<Sheet> sheets = _context.Sheets.Where(x => x.Id == id).ToList();
        if (sheets.Count == 0) {
            return null;
        }
        else {
            return sheets[0];
        }
    }

    /// <summary>
    /// Get all sheets from the database with a given name
    /// </summary>
    /// <param name="name">Name of the sheet</param>
    /// <returns>List of all sheets with the name</returns>
    public List<Sheet> GetSheets(string name) {
        return _context.Sheets.Where(x => x.Name == name).ToList();
    }
    
    /// <summary>
    /// Add result to the database
    /// </summary>
    /// <param name="wins">Number of wins</param>
    /// <param name="losses">Number of losses</param>
    /// <param name="comments">Comment string</param>
    /// <param name="tags">List of tags</param>
    public void AddResult(int wins, int losses, string comments, List<string> tags) {
        Result result = new Result(wins, losses, comments, tags);
        _context.Results.Add(result);
    }
    
    /// <summary>
    /// Add Result object to the database
    /// </summary>
    /// <param name="result">Result object <see cref="Result"/>></param>
    public void AddResult(Result result) {
        _context.Results.Add(result);
    }
    
    /// <summary>
    /// Add sheet to the database
    /// </summary>
    /// <param name="name">Name of the sheet entry</param>
    /// <param name="spreadsheetId">ID of the Google Sheet</param>
    /// <param name="pathToCredentials">Path to JSON credentials</param>
    public void AddSheet(string name, string spreadsheetId, string pathToCredentials) {
        Sheet sheet = new Sheet(name, spreadsheetId, pathToCredentials);
        _context.Sheets.Add(sheet);
    }
    
    /// <summary>
    /// Delete specified result from the database
    /// </summary>
    /// <param name="id">ID of the result to delete</param>
    public void DeleteResult(int id) {
        Result? result = GetResult(id);
        if (result != null) {
            _context.Results.Remove(result);
        }
    }

    /// <summary>
    /// Delete specified result from the database
    /// </summary>
    /// <param name="result">Result object <see cref="Result"/>></param>
    public void DeleteResult(Result result) {
        _context.Results.Remove(result);
    }
    
    /// <summary>
    /// Delete specified sheet entry from the database
    /// </summary>
    /// <param name="id">ID of the sheet entry</param>
    public void DeleteSheet(int id) {
        Sheet? sheet = GetSheet(id);
        if (sheet != null) {
            _context.Sheets.Remove(sheet);
        }
    }

    /// <summary>
    /// List results from the database based on search criteria
    /// </summary>
    /// <param name="result">String specifying result (win/loss/draw), + operator can be used</param>
    /// <param name="tags">Tags that the result should have</param>
    /// <param name="year">Year filter</param>
    /// <param name="month">Month filter</param>
    /// <param name="day">Day filter</param>
    /// <param name="comments">Comment filter (inclusion, not exact match)</param>
    /// <returns>List of results satisfying the requirements</returns>
    public List<Result> ListResults(string? result, List<string>? tags, int? year, int? month, int? day,
        string? comments) {
        var conditions = new List<Func<Result, bool>>();
        
        if (result != null) {
            Tuple<bool, bool, bool> wantedResult = _InterpretWantedResult(result);
            conditions.Add(x => 
                (wantedResult.Item1 && x.IsWin()) || 
                (wantedResult.Item2 && x.IsLoss()) || 
                (wantedResult.Item3 && x.isDraw()));
        }
        
        if (tags != null) {
            conditions.Add(x => tags.All(tag => x.Tags.Contains(tag)));
        }
        
        if (year != null) {
            conditions.Add(x => x.Timestamp.Year == year);
        }
        
        if (month != null) {
            conditions.Add(x => x.Timestamp.Month == month);
        }
        
        if (day != null) {
            conditions.Add(x => x.Timestamp.Day == day);
        }
        
        if (comments != null) {
            conditions.Add(x => x.Comments.Contains(comments));
        }
        
        List<Result> results = new List<Result>();
        foreach (Result match in _context.Results) {
            if (conditions.All(condition => condition(match))) {
                results.Add(match);
            }
        }
        
        return results;
    }

    /// <summary>
    /// Save the database contents
    /// </summary>
    public void Save() {
        _context.SaveChanges();
    }
    
    /// <summary>
    /// Safely dispose of the database context
    /// </summary>
    public void Dispose() {
        _context.SaveChanges();
        _context.Dispose();
    }
    
    private Tuple<bool, bool, bool> _InterpretWantedResult(string? result) {
        bool win = false;
        bool loss = false;
        bool draw = false;
        
        if (result != null) {
            string[] wantedResults = result.Split('+');
            foreach (string wantedResult in wantedResults) {
                if (wantedResult == "win") {
                    win = true;
                }
                else if (wantedResult == "loss") {
                    loss = true;
                }
                else if (wantedResult == "draw") {
                    draw = true;
                }
            }
        }
        
        return new Tuple<bool, bool, bool>(win, loss, draw);
    } 
}