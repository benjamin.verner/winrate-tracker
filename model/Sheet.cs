namespace WinRateTracker.model;

/// <summary>
/// Class representing a sheet in a Google Sheets document. Used for syncing data between the application and
/// Google Sheets.
/// </summary>
public class Sheet {
    /// <summary>
    /// ID of the sheet object. Used for identifying the sheet in the database.
    /// </summary>
    public int Id { get; init; }
    
    /// <summary>
    /// Name of the sheet object. Can be used to identify the sheet bind rather than by ID.
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// ID of the related Google Sheets document. Can be found in its URL.
    /// </summary>
    public string SpreadsheetId { get; set; }
    /// <summary>
    /// Path to the JSON credentials file for the Google Sheets API. Used for authenticating the application.
    /// </summary>
    public string PathToCredentials { get; set; }
    
    public Sheet(string name, string spreadsheetId, string pathToCredentials) {
        Name = name;
        SpreadsheetId = spreadsheetId;
        PathToCredentials = pathToCredentials;
    }
}