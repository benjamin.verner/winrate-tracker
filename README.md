# WinRate Tracker

**WinRate Tracker** is a console application designed to track a player's game results and display various
statistics about their performance. It's primarily intended for _Magic: The Gathering_ players, but it can
be used for any game that has a win/loss outcome.

Features include adding games to the tracker, grouping tracked games by tags, listing and filtering 
tracked games, updating game results, and displaying detailed performance statistics.
The entire database can be synchronised with a _Google Sheets_ document, allowing for easy sharing and backup.

## Table of Contents

- [Usage](#user-content-usage)
  - [Getting Started](#user-content-getting-started)
  - [Statistics](#user-content-statistics)
  - [Synchronizing with Google Sheets](#user-content-synchronizing-with-google-sheets)
  - [Commands](#user-content-commands)

## Usage

### Getting Started

The application is a C# console application, and the included .csproj file allows it to be compiled and run using
.NET 8, for example using the `dotnet` command.

### Statistics

The application can display various statistics about the player's performance. Currently, the following
statistics are implemented:

- **Total Matches**: The total number of matches played.
- **Total Match Wins**: The total number of matches won.
- **Total Match Losses**: The total number of matches lost.
- **Match Win Rate**: The percentage of matches won (excluding draws).
- **Total Games**: The total number of games played.
- **Total Game Wins**: The total number of games won.
- **Total Game Losses**: The total number of games lost.
- **Game Win Rate**: The percentage of games won (excluding draws).
- **Longest Win Streak**: The longest streak of wins without a loss (draws are ignored as they might indicate
  a split in the tournament, for example).
- **Most Common Tags**: The tags that appear most frequently in the tracked games.
- **Win Rate by Tag**: The match win rate for each tag.
- **Win Rate by Month**: The match win rate for each month.

![stats](./img/stats.png)

### Synchronizing with Google Sheets

To synchronise the application with a _Google Sheets_ document, you must provide the application with
credentials to access the document. This is done by creating a new project in the _Google Cloud Platform_,
enabling the _Google Sheets API_ for the project, and downloading the credentials as a JSON file. The path to
this file must be provided as an argument when binding the application to a _Google Sheets_ document.

Additionally, you must provide the ID of the _Google Sheets_ document that you want to bind the application to.
This ID can be found in the URL of the document.

### Commands

The application is controlled using a set of subcommands. This section explains the syntax and usage of each
available subcommand. A short description can also be displayed by running the application with the `--help` flag.

- **create-db**
  - Creates a new database file named `mtg.db` in the current directory, if it doesn't already exist.
  - **Syntax:** `./winratetracker create-db`
- **add**
  - Adds a new result to the database with the specified number of wins and losses. Tags and comments can be
    added to the game for easier filtering and categorisation.
  - **Syntax:** `./winratetracker add <number-of-wins>  <number-of-losses> [options]`
  - **Options:**
    - `--tags <tags>`: Space-separated list of string tags to assign to the game.
    - `--comment <comment>`: A comment to add to the game.
- **get**
  - Displays detailed information about a result with the specified ID. The ID can be obtained by listing the
    games in the database.
  - **Syntax:** `./winratetracker get <result-id>`
- **list**
  - Lists information about all results in the database, optionally filtered by various criteria.
  - **Syntax:** `./winratetracker list [options]`
  - **Options:**
    - `--result <result>`: Filter the games by result (win/loss/draw), operator `+` can be used (e.g. `win+draw`).
    - `--tags <tags>`: Space-separated list of tags to filter the games by (all tags must be present).
    - `--year <year>`: Filter the games by year.
    - `--month <month>`: Filter the games by month.
    - `--day <day>`: Filter the games by day.
    - `--comment <comment>`: Filter the games by comment (phrase must be present in the comment).
- **update**
  - Updates a result already present in the database. The result ID must be specified, and the options control
    what information is updated.
  - **Syntax:** `./winratetracker update <result-id> [options]`
  - **Options:**
    - `--delete`: Delete the result instead of updating it.
    - `--wins <number-of-wins>`: Update the number of wins in the result.
    - `--losses <number-of-losses>`: Update the number of losses in the result.
    - `--comment <comment>`: Update the comment of the result.
    - `--add-tags <tags>`: Add tags to the result.
    - `--replace-tags <tags>`: Replace all tags in the result with the specified tags.
- **stats**
  - Displays various statistics about the player's performance, as described in the _Statistics_ section.
  - **Syntax:** `./winratetracker stats`
- **bind-sheet**
  - Binds the application to a _Google Sheets_ document, allowing for synchronisation of the database with the
    document.
  - **Syntax:** `./winratetracker bind-sheet <name> <sheet-id> <path-to-credentials>`
    - \<name> does not have to correspond to the actual name of the document, it serves as potential identifier.
- **remove-sheet**
  - Removes the binding between the application and a _Google Sheets_ document.
  - **Syntax:** `./winratetracker remove-sheet <sheet-id>`
- **list-sheets**
  - Lists all _Google Sheets_ documents that the application is bound to.
  - **Syntax:** `./winratetracker list-sheets`
- **mirror-sheet**
  - Updates the local database to be a copy of the _Google Sheets_ document with the specified ID or Name (if the 
    name is unique). Only one of the two parameters can be used.
  - **Syntax:** `./winratetracker mirror-sheet <name> <id>`
- **write-to-sheet**
  - Updates the _Google Sheets_ document with the specified ID or Name (if the name is unique) to be a copy of the
    local database. Only one of the two parameters can be used.
  - **Syntax:** `./winratetracker write-to-sheet <name> <id>`
