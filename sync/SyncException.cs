namespace WinRateTracker.sync;

/// <summary>
/// Exception class for handling sync errors
/// </summary>
[Serializable]
public class SyncException : Exception {
    public SyncException(string message) : base(message) { }
}