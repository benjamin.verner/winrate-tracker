using WinRateTracker.model;
using WinRateTracker.view;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;

namespace WinRateTracker.sync;

/// <summary>
/// Class for syncing results with a Google Sheets document. Includes read/write functionality.
/// </summary>
public class GoogleSheetsSyncer {
    private SheetsService _service { get; set; }
    const string ApplicationName = "WinRate Tracker";
    private string _spreadsheetId;
    static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
    

    public GoogleSheetsSyncer(string name, string spreadsheetId, string credentialPath) {
        _service = _InitializeService(credentialPath);
        _spreadsheetId = spreadsheetId;
    }

    /// <summary>
    /// Get results from the Google Sheet and return them as a dictionary of (row number, Result) pairs.
    /// </summary>
    /// <returns>Dictionary (int, Result) of row number, result pairs <see cref="Result"/></returns>
    /// <throws>SyncException if an error occurs during reading</throws>
    public Dictionary<int, Result> GetResultsFromSheet() {
        var sheetValues = _GetSheetValues();
        Dictionary<int, Result> results = new();

        int rowNumber = 1;
        foreach (var row in sheetValues) {
            try {
                results[rowNumber] = Result.FromSheetRow(row);
            }
            catch (Exception e) {
                ErrorDisplayer.DisplayError($"Error parsing row {rowNumber}: " + e.Message);
            }
            rowNumber++;
        }
        return results;
    }

    /// <summary>
    /// Write the provided results to the Google Sheet.
    /// </summary>
    /// <param name="results">Dictionary of (row number, Result) pairs specifying the row a result should be written to <see cref="Result"/></param>
    /// <throws>SyncException if an error occurs during writing</throws>
    public void WriteResultsToSheet(Dictionary<int, Result> results) {
        var data = new List<ValueRange>();
        foreach (var (row, result) in results) {
            data.Add(_CreateValueRange(result, row));
        }
        
        _WriteBatch(data);
    }
    
    private SheetsService _InitializeService(string credentialPath) {
        var credential = _GetCredentialFromFile(credentialPath);
        return new SheetsService(new BaseClientService.Initializer {
            HttpClientInitializer = credential,
            ApplicationName = ApplicationName
        });
    }
    
    private GoogleCredential _GetCredentialFromFile(string credentialPath) {
        using var stream = new FileStream(credentialPath, FileMode.Open, FileAccess.Read);
        return GoogleCredential.FromStream(stream)
            .CreateScoped(Scopes);
    }

    private IList<IList<object>> _GetSheetValues() {
        string range = "Results!A1:F";
        
        SpreadsheetsResource.ValuesResource.GetRequest request =
            _service.Spreadsheets.Values.Get(_spreadsheetId, range);

        try {
            ValueRange response = request.Execute();
            return response.Values;
        }
        catch (Google.GoogleApiException e) {
            throw new SyncException("Google API Exception: " + e.Message);
        }
        catch (HttpRequestException e) {
            throw new SyncException("HTTP Request Exception: " + e.Message);
        }
        catch (Exception e) {
            throw new SyncException("Exception: " + e.Message);
        }
    }

    private ValueRange _CreateValueRange(Result result, int row) {
        return new ValueRange {
            Range = $"Results!A{row}:F{row}",
            Values = new List<IList<object>> {
                new List<object> {
                    result.Id,
                    result.Wins,
                    result.Losses,
                    result.Timestamp,
                    result.Comments,
                    string.Join(",", result.Tags)
                }
            }
        };
    }
    
    private void _WriteBatch(List<ValueRange> data) {
        var request = new BatchUpdateValuesRequest {
            Data = data,
            ValueInputOption = "RAW"
        };

        try {
            _service.Spreadsheets.Values.BatchUpdate(request, _spreadsheetId).Execute();
        }
        catch (Google.GoogleApiException e) {
            throw new SyncException("Google API Exception: " + e.Message);
        }
        catch (HttpRequestException e) {
            throw new SyncException("HTTP Request Exception: " + e.Message);
        }
        catch (Exception e) {
            throw new SyncException("Exception: " + e.Message);
        }
    }
    
}